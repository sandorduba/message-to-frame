import { Parent } from "./parent";

describe('Parent class tests', () => {
    it('exists / smoke test', () => {
        const parent = new Parent(window);
        expect(parent).toBeDefined();
    });

    describe('constructor test', () => {
        let consoleErrorSpy: jest.SpyInstance;

        beforeAll(() => {
            consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();
        });

        afterAll(() => {
            consoleErrorSpy.mockRestore();
        });

        it('should throw error on null or undefined', () => {
            let parent = null;
            try {
                parent = new Parent(null);
            } catch (e) {
                console.error(e);
            }
            expect(console.error).toHaveBeenCalled();
            expect(parent).toBe(null);
        });
    });
});
