export class Parent {
    private childWindow: Window;

    constructor(childWindow: Window) {
        if (!childWindow) throw new Error('Window must be specified.');
        this.childWindow = childWindow;
    }
}
