export class Child {
    private parentWindow: Window;

    constructor(parentWindow: Window) {
        if (!parentWindow) throw new Error('Window must be specified.');
        this.parentWindow = parentWindow;
    }
}
