import { Child } from "./child";

describe('Child class tests', () => {
    it('exists / smoke test', () => {
        const child = new Child(window);
        expect(child).toBeDefined();
    });

    describe('constructor test', () => {
        let consoleErrorSpy: jest.SpyInstance;

        beforeAll(() => {
            consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();
        });

        afterAll(() => {
            consoleErrorSpy.mockRestore();
        });

        it('should throw error on null or undefined', () => {
            let child = null;
            try {
                child = new Child(null);
            } catch (e) {
                console.error(e);
            }
            expect(console.error).toHaveBeenCalled();
            expect(child).toBe(null);
        });
    });
});
